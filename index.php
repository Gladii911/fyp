
<?php require 'header_foot.php';?>
	<div id="da-slider" class="da-slider" style="margin-top:200px;float:bottom;">
				<div class="da-slide">
					<h2>Real Time Activity Recognition</h2>
					<p>Datasets provide realtime human activities recognition, activities include walking,jogging,running,jumping etc</p>
					
					<div class="da-img"><img src="Static/images/reco.png" alt="image01" /></div>
				</div>
				<div class="da-slide">
					<h2>94% accuraccy</h2>
					<p> Our Dataset provides 94.12% accuracy using naive bayes algorithm </p>
					
					<div class="da-img"><img src="Static/images/accurate-data.png" alt="image01" /></div>
				</div>
				<div class="da-slide">
					<h2>Free Datasets Provided</h2>
					<p> We provide Free Datasets which include Raw dataset, Median filtered Dataset and feature extracted dataset. <br>for more info <a href="about_us.php">click</a> </p>
					<div class="da-img"><img src="Static/images/run.png" alt="image01" style="width:60%;" /></div>
				</div>
				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
	</div>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="Static/js/jquery.cslider.js"></script>
		<script type="text/javascript">
			$(function() {
			
				$('#da-slider').cslider({
					autoplay	: true,
					bgincrement	: 450
				});
			
			});
		</script>	
	
</body>
</html>