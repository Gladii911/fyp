<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>About Us</title>
		</head>
		<?php require 'header_foot.php';?>
			<div class="container" style="background-color:white;width:80%;margin-top:110px;z-index:-1;">
				<h3>Building Android App Calorie Meter based on Human Activity Recognition using Smartphones</h3>	
				<h4 class="subheadings"> Introduction</h4>
					<p class="paras_in_aboutus">
						Human activity recognition is an important yet challenging
						research area with many applications in healthcare, smart
						environments, and homeland security. Computer vision-based
						techniques and wearable sensors can be used for human
						activity recognition, but each have their own disadvantages.
						Cameras are immobile and have storage problems since the
						video generated consumes a lot of space, whereas wearable
						sensors are obtrusive and uncomfortable in daily life.
						Smartphones consists of many hardware sensors embedded in
						them, such as accelerometer, gyroscope, orientation sensor etc.
						The role of smartphones for automatic activity recognition can
						have several advantages due to easy device portability, and no
						requirement for additional fixed equipment that could be
						obtrusive and uncomfortable to the user. If automatic activity
						recognition systems can be built based on intelligent
						processing of sensor features on smartphones, it will be a great
						contribution to health area, particularly for calorie burning
						tracking, remote activity monitoring and recognition in aged
						care and disability care sector. 
					</p>
				<h5 class="subheadings">SENSORS IN ANDROID</h5>
				<p class="paras_in_aboutus">
					Almost all Android smartphone devices contain various
					sensors integrated in them. The Android platform supports
					three broad categories of sensors, namely, motion sensors,
					environmental sensors and position sensors. Motion sensors
					measure acceleration forces and rotational forces along three
					axes. This category includes accelerometers, gravity sensors,
					gyroscopes, and rotational vector sensors. Environmental
					sensors measure various environmental parameters, such as
					ambient air temperature and pressure, illumination, and
					humidity. This category includes barometers, photometers,
					and thermometers. Position sensors measure the physical
					position of a device. This category includes orientation sensors
					and magnetometers. We are particularly interested in
					accelerometer sensor, which measures the acceleration force
					in m/s2
					 that is applied to a device on all three physical axes (x,
					y, and z), including the force of gravity.
				</p>


				<h4 class="subheadings">Dataset</h4>
			
				<p class="paras_in_aboutus">
					To gather our own data, we first built a temporary Android
					application that will record the readings of smartphone’s
					accelerometer sensor which will be related to the user’s
					movements. The Android application contained options to
					select the activity and a Start and Stop button. Each group
					member was instructed to first select the activity option, hold 
					4
					the smartphone in the right hand, press the Start button and do
					the activity for minimum 2 minutes and then press the Stop
					button. We captured triaxial acceleration from the
					accelerometer (that is, along all 3 axes X, Y and Z) at a
					frequency of 250Hz. Every group member performed the
					following activities and the activities were labelled explicitly.
					

				</p>
				<p class="paras_in_aboutus">
					The activities we recorded are as follows: <br>
						<ol>
							<li> Idle (Standing)</li>
							<li>Slow walking</li>
							<li> Normal walking</li>
							<li>Fast walking</li>
							<li>Jogging</li>
							<li>Running</li>
							<li>Jumping</li>
						</ol><br>
				</p>
				<p class="paras_in_aboutus">
							Hence, by performing each activity for 3 minutes by all
						group members, we have a rich amount of labelled dataset.
						This labelled dataset will serve for training and testing of our
						model.
						</p>
				<p class="paras_in_aboutus">
					The following graphs depicts reflect the nature of readings
					of the accelerometer sensor across all 3 axes:
					X-axis: Activity <br>
					(0-100 : Idle <br>
					101-200 : Slow Walking <br>
					201-300 : Normal Walking <br>
					301-400 : Fast Walking <br>
					401-500 : Jogging <br>
					501-600 : Running <br>
					601-700 : Jumping) <br>
					<br>
					<img src="Static/images/graph1.png" alt="">
				</p>
				<p class="paras_in_aboutus">
					From the graphs, it can be inferred that activities in which
					mobile movement is low such as slow walking or idle results
					is least amount of fluctuation in the readings while activities in
					which mobile movement is high such as jogging, running or
					jumping shows high amount of fluctuation in the readings.
					Such fluctuations will be helpful for the machine learning
					algorithms to classify the readings.
				</p>

				<h4 class="subheadings">Noise Reduction</h4>
				<p class="paras_in_aboutus">
				For noise reduction, the readings were preprocessed with
				median filter. A graph of some raw readings versus
				corresponding pre-processed readings using median filter is
				depicted below. <br>
				X-axis: Activity<br>
				Y-axis: Acceleration in m/s2<br>
				- Raw readings.<br>
				- Pre-processed readings with median filter. <br>
				<img src="Static/images/graph2.png" alt="">
				</p>
				<p class="paras_in_aboutus">
					It can be inferred from the graph that due to median filter,
					extreme values that tend to be outliers are eliminated.
					It is also important to know whether median filter increases
					or decreases the accuracy. Hence we compared both raw and
					preprocessed (median filter) dataset for their accuracy using
					various machine learning algorithms. Both the datasets were first randomized and then divided into two datasets, training
					(70%) and testing (30%). The following table summarizes the
					testing.
					Hence, it can be observed that median filter helps in
					increasing the accuracy of all the above machine learning
					algorithms.



				</p>
				<pre>
					Machine Learning      Algorithm Accuracy for      Accuracy for Preprocessed Dataset
					                      Raw Dataset(inpercentage)	  using median filter (in percentage)
	      Naïve Bayes           71.4544                     72.9681
	      J48 Decision Tree     76.9347                     82.1192
	      Random Forest         77.8681                     86.3034
              Bagging               78.2294                     83.4136
             IBk                   73.1707                     81.186
              Support Vector Machine 61.0659                     68.8742 
            
					
				</pre>

		<h4 class="subheadings">V. FEATURE EXTRACTION</h4>
		<p class="paras_in_aboutus">
			Feature extraction starts from an initial set of measured data
			and builds derived values (features) intended to be informative
			and non-redundant, facilitating the subsequent learning and
			generalization steps, and in some cases leading to better
			interpretations. Accelerometer signals are highly fluctuating
			and oscillatory, which makes it difficult to recognize the
			underlying patterns using their raw values. Existing HAR
			systems based on accelerometer data employ statistical feature
			extraction and, in most of the cases, either time or frequency
			domain features. Hence we use the following methods to
			extract features:

			<img src="Static/images/`formulae.png" alt=""><br>
			We used a window for every axis that consists of 8
			consecutive pre-processed (using median filter) readings from
			accelerometer for that corresponding axis. Hence, for the 3
			axes, we have 3 windows. For each window, we calculated the
			features using the above mentioned methods to get a single
			value that against those 8 consecutive readings. From the same
			window, we derive new 8 values of those 8 consecutive
			readings using Fast Fourier Transform. We then calculate the
			features using the above mentioned methods to get a single
			value that against those 8 Fast Fourier transform readings.
			Hence, we have 42 features (21 features of normal readings of
			acceleration across x, y z axis; 21 features of fast Fourier
			transform of the readings of acceleration across x, y z axis).
			
		</p>
		<h4 class="subheadings">FEATURE SELECTION AND TESTING OF MACHINE
		LEARNING ALGORITHMS</h4>
		<p class="paras_in_aboutus">
		Some features in the dataset might contain redundant or
		irrelevant information that can negatively affect the
		recognition accuracy. Then, implementing techniques for
		selecting the most appropriate features is a suggested practice
		to reduce computations and simplify the learning models. We
		have generated 42 features. Including all these features will
		have the following disadvantages: <br>
		• Some features will contribute to a negligible extent or
		may not contribute at all while generating a learning
		model using a specific machine learning algorithm.<br>
		• It will increase the undue processing of the processor
		and thus use more battery.<br>
		• It will increase the response time.<br>
		</p>
		<p class="paras_in_aboutus">
		Hence, we will select only a subset of those features that
		contribute most to the learning. This depends on the type of
		machine learning algorithm to be used.
		The training and testing were carried out using the feature
		extracted dataset (containing above mentioned 42 features and
		1 activity class attribute). This dataset contains 1265
		observations/instances. We first randomized the observations.
		We then divided the dataset into two datasets, namely training
		set and testing set. The training set constituted 70% and the
		testing set constituted 30% of the randomized feature
		extracted dataset. Various machine learning algorithms were
		tested for accuracy and model building time, using all as well
		as a set of important features. The selection of machine
		learning algorithm criteria is higher accuracy, less number of
		features, and less model building time.
		</p>
		</div>
	</body>
</html>