<?php 
	include "header_foot.php";
?>
<body>
	<div style="margin-top:110px;width:90%;margin-left:auto;margin-right:auto;">
		<h2>Download our dataset</h2>	
		
		<table class="table table-condensed" style="width:49%;float:left;border:2px solid white;">
		    <thead>
		    	<tr>
		    		<th>In hand</th>
		    	</tr>
		      <tr>
		        <th>Raw dataset</th>
		        <th>Median Filtered dataset</th>
		        <th>Feature Extracted dataset</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php
		    	$directory_raw = 'Static/Data/inside/Raw';
				$scanned_directory_raw = array_diff(scandir($directory_raw), array('..', '.'));

				$directory_mid = 'Static/Data/inside/Medium';
				$scanned_directory_med = array_diff(scandir($directory_mid), array('..', '.'));

				$directory_fet = 'Static/Data/inside/featureex';
				$scanned_directory_fet = array_diff(scandir($directory_fet), array('..', '.'));

				$x= sizeof($scanned_directory_raw);
				
			while ( $x>=2) {
				echo"
					  <tr>
		         <td><a style='color:black;' href='downloadlink.php?name=".$scanned_directory_raw[$x]."'>".$scanned_directory_raw[$x]."</a></td>
		       	 <td><a style='color:black;' href='downloadlink.php?name=".$scanned_directory_med[$x]."'>".$scanned_directory_med[$x]."</a></td>
		       	  <td><a style='color:black;' href='downloadlink.php?name=".$scanned_directory_fet[$x]."'>".$scanned_directory_fet[$x]."</a></td>
		      </tr>
				";
				$x--;
			}
		?>
		    
		     
		    </tbody>
		  </table>

		  <table class="table table-condensed" style="width:49%;border:2px solid white;float:right;">
		    <thead>
		    	<tr>
		    		<th>In Pocket</th>
		    	</tr>
		      <tr>
		        <th>Raw dataset</th>
		        <th>Median Filtered dataset</th>
		        <th>Feature Extracted dataset</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php
		    	$directory_raw = 'Static/Data/outside/Raw';
				$scanned_directory_raw = array_diff(scandir($directory_raw), array('..', '.'));

				$directory_mid = 'Static/Data/outside/Medium';
				$scanned_directory_med = array_diff(scandir($directory_mid), array('..', '.'));

				$directory_fet = 'Static/Data/outside/featureex';
				$scanned_directory_fet = array_diff(scandir($directory_fet), array('..', '.'));

				$x= sizeof($scanned_directory_raw);
				
			while ( $x>=2) {
				echo"
					  <tr>
		         <td><a style='color:black;' href='downloadlink.php?name=".$scanned_directory_raw[$x]."'>".$scanned_directory_raw[$x]."</a></td>
		       	 <td><a style='color:black;' href='downloadlink.php?name=".$scanned_directory_med[$x]."'>".$scanned_directory_med[$x]."</a></td>
		       	  <td><a style='color:black;' href='downloadlink.php?name=".$scanned_directory_fet[$x]."'>".$scanned_directory_fet[$x]."</a></td>
		      </tr>
				";
				$x--;
			}
		?>
		    
		     
		    </tbody>
		  </table>
	</div>
	
</body>
</html>