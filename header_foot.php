<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Human Activity Recognition</title>
	<link rel="stylesheet" href="Static/css/bootstrap.min.css">
    <script src="Static/js/jquery.min.js"></script>
    <link rel="stylesheet" href="Static/css/index.css">
    <link rel="stylesheet" type="text/css" href="Static/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="Static/css/style2.css" />
	<script type="text/javascript" src="Static/js/modernizr.custom.28468.js"></script>
</head>
<body class="container-fluid" style="margin:0px;padding:0px;">
	<div class="header container-fluid" style="position:fixed;width:100%;">
		<img src="Static/images/Logo.png" alt="logo" id="logo">
		<h3 class="title" style="margin:0px;font-weight:bold;margin-left:8%;padding-top:0.5%;">Human Activity Recognition</h3>
			<nav class="navbar navbar-default" style="background:none;border:0px;float:bottom;margin:0px;">
			  <div class="" style="margin-top: 1%;margin-left: 8%;font-weight:bold;font-size:16px;">
			    <div class="navbar-header">
			      <a class="navbar-brand menu" href="index.php">Home</a>
			    </div>
			    <ul class="nav navbar-nav"  >
			      <li class="menu"><a href="about_us.php">About</a></li>
			      <li class="menu"><a href="Downloads.php">Downloads</a></li>
			    </ul>
			  </div>
			</nav>
	</div>