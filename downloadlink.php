<?php
$FileName =  $_GET['name'];
$FilePath = "Static/data";  

$size = filesize($FilePath . $FileName) ;
header("Content-Type: application/force-download; name=\"". $FileName ."\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ". $size ."");
header("Content-Disposition: attachment; filename=\"". $FileName ."\"");
header("Expires: 0");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
echo (readfile($FilePath . $FileName));
?>
